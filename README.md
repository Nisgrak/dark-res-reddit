# Dark RES Reddit

Companion theme for [RES](https://redditenhancementsuite.com/) night theme

## 👀 Preview
![Dark RES REddit preview](./images/preview.png)

## 🚀 Installation

A userstyle extension is required, common ones include:

🎨 Stylus for [Firefox](https://addons.mozilla.org/en-US/firefox/addon/styl-us/), [Chrome](https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne) or [Opera](https://addons.opera.com/en-gb/extensions/details/stylus/).<br>
🎨 xStyle for [Firefox](https://addons.mozilla.org/firefox/addon/xstyle/) or [Chrome](https://chrome.google.com/webstore/detail/xstyle/hncgkmhphmncjohllpoleelnibpmccpj).

Then:

📦 [Install the usercss](https://gitlab.com/Nisgrak/dark-res-reddit/-/raw/master/dark-res-reddit.user.css). It supports automatic updates.<br>